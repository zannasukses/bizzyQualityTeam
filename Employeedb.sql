select a.employee_id, a.employee_name, b.buddy_name, s.supervisor_name, t.team_name
from employee a join buddy b on a.buddy_id = b.buddy_id
join supervisor s on a.supervisor_id = s.supervisor_id
join team t on a.team_name = t.team_name;

